package com.sillysocialdemo.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FeedListResponseModel {
    public Object Content;
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("success")
    @Expose
    private Integer success;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public class Content {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("createdAt")
        @Expose
        private String createdAt;
        @SerializedName("feedType")
        @Expose
        private Integer feedType;
        @SerializedName("mediaType")
        @Expose
        private Integer mediaType;
        @SerializedName("textDescription")
        @Expose
        private String textDescription;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("video")
        @Expose
        private String video;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("avatar")
        @Expose
        private String avatar;
        @SerializedName("userId")
        @Expose
        private Integer userId;
        @SerializedName("totalLikes")
        @Expose
        private Integer totalLikes;
        @SerializedName("totalComments")
        @Expose
        private Integer totalComments;
        @SerializedName("isLiked")
        @Expose
        private Integer isLiked;
        @SerializedName("trendingFactor")
        @Expose
        private Integer trendingFactor;
        @SerializedName("comment")
        @Expose
        private Comment comment;
        @SerializedName("feedHashtag")
        @Expose
        private List<Object> feedHashtag = null;
        @SerializedName("feedYoutag")
        @Expose
        private List<FeedYoutag> feedYoutag = null;
        @SerializedName("media")
        @Expose
        private List<Object> media = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public Integer getFeedType() {
            return feedType;
        }

        public void setFeedType(Integer feedType) {
            this.feedType = feedType;
        }

        public Integer getMediaType() {
            return mediaType;
        }

        public void setMediaType(Integer mediaType) {
            this.mediaType = mediaType;
        }

        public String getTextDescription() {
            return textDescription;
        }

        public void setTextDescription(String textDescription) {
            this.textDescription = textDescription;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getTotalLikes() {
            return totalLikes;
        }

        public void setTotalLikes(Integer totalLikes) {
            this.totalLikes = totalLikes;
        }

        public Integer getTotalComments() {
            return totalComments;
        }

        public void setTotalComments(Integer totalComments) {
            this.totalComments = totalComments;
        }

        public Integer getIsLiked() {
            return isLiked;
        }

        public void setIsLiked(Integer isLiked) {
            this.isLiked = isLiked;
        }

        public Integer getTrendingFactor() {
            return trendingFactor;
        }

        public void setTrendingFactor(Integer trendingFactor) {
            this.trendingFactor = trendingFactor;
        }

        public Comment getComment() {
            return comment;
        }

        public void setComment(Comment comment) {
            this.comment = comment;
        }

        public List<Object> getFeedHashtag() {
            return feedHashtag;
        }

        public void setFeedHashtag(List<Object> feedHashtag) {
            this.feedHashtag = feedHashtag;
        }

        public List<FeedYoutag> getFeedYoutag() {
            return feedYoutag;
        }

        public void setFeedYoutag(List<FeedYoutag> feedYoutag) {
            this.feedYoutag = feedYoutag;
        }

        public List<Object> getMedia() {
            return media;
        }

        public void setMedia(List<Object> media) {
            this.media = media;
        }
    }
    public class Comment {

        @SerializedName("commentId")
        @Expose
        private Integer commentId;
        @SerializedName("commentedUserId")
        @Expose
        private Object commentedUserId;
        @SerializedName("commentedUserFname")
        @Expose
        private String commentedUserFname;
        @SerializedName("commentedUserLname")
        @Expose
        private String commentedUserLname;
        @SerializedName("commentedUserAvatar")
        @Expose
        private String commentedUserAvatar;
        @SerializedName("comment")
        @Expose
        private String comment;
        @SerializedName("commentCreatedAt")
        @Expose
        private String commentCreatedAt;

        public Integer getCommentId() {
            return commentId;
        }

        public void setCommentId(Integer commentId) {
            this.commentId = commentId;
        }

        public Object getCommentedUserId() {
            return commentedUserId;
        }

        public void setCommentedUserId(Object commentedUserId) {
            this.commentedUserId = commentedUserId;
        }

        public String getCommentedUserFname() {
            return commentedUserFname;
        }

        public void setCommentedUserFname(String commentedUserFname) {
            this.commentedUserFname = commentedUserFname;
        }

        public String getCommentedUserLname() {
            return commentedUserLname;
        }

        public void setCommentedUserLname(String commentedUserLname) {
            this.commentedUserLname = commentedUserLname;
        }

        public String getCommentedUserAvatar() {
            return commentedUserAvatar;
        }

        public void setCommentedUserAvatar(String commentedUserAvatar) {
            this.commentedUserAvatar = commentedUserAvatar;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getCommentCreatedAt() {
            return commentCreatedAt;
        }

        public void setCommentCreatedAt(String commentCreatedAt) {
            this.commentCreatedAt = commentCreatedAt;
        }

    }
    public class FeedYoutag {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("feedId")
        @Expose
        private Integer feedId;
        @SerializedName("youtagId")
        @Expose
        private Integer youtagId;
        @SerializedName("createdAt")
        @Expose
        private String createdAt;
        @SerializedName("updatedAt")
        @Expose
        private String updatedAt;
        @SerializedName("deletedAt")
        @Expose
        private Object deletedAt;
        @SerializedName("tags")
        @Expose
        private Tags tags;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getFeedId() {
            return feedId;
        }

        public void setFeedId(Integer feedId) {
            this.feedId = feedId;
        }

        public Integer getYoutagId() {
            return youtagId;
        }

        public void setYoutagId(Integer youtagId) {
            this.youtagId = youtagId;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(Object deletedAt) {
            this.deletedAt = deletedAt;
        }

        public Tags getTags() {
            return tags;
        }

        public void setTags(Tags tags) {
            this.tags = tags;
        }

    }
    public class Pagination {

        @SerializedName("page")
        @Expose
        private Integer page;
        @SerializedName("limit")
        @Expose
        private String limit;

        public Integer getPage() {
            return page;
        }

        public void setPage(Integer page) {
            this.page = page;
        }

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

    }
    public static class Result {

        @SerializedName("contentList")
        @Expose
        private List<Content> contentList = null;
        @SerializedName("pagination")
        @Expose
        private Pagination pagination;
        @SerializedName("outerOffset")
        @Expose
        private Integer outerOffset;

        public List<Content> getContentList() {
            return contentList;
        }

        public void setContentList(List<Content> contentList) {
            this.contentList = contentList;
        }

        public Pagination getPagination() {
            return pagination;
        }

        public void setPagination(Pagination pagination) {
            this.pagination = pagination;
        }

        public Integer getOuterOffset() {
            return outerOffset;
        }

        public void setOuterOffset(Integer outerOffset) {
            this.outerOffset = outerOffset;
        }

    }
    public class Tags {

        public boolean isPosition() {
            return position;
        }

        public void setPosition(boolean position) {
            this.position = position;
        }

        @SerializedName("indexposition")
        @Expose
        private boolean position;

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("text")
        @Expose
        private String text;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("userId")
        @Expose
        private Object userId;
        @SerializedName("isApproved")
        @Expose
        private Integer isApproved;
        @SerializedName("createdAt")
        @Expose
        private String createdAt;
        @SerializedName("updatedAt")
        @Expose
        private String updatedAt;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Object getUserId() {
            return userId;
        }

        public void setUserId(Object userId) {
            this.userId = userId;
        }

        public Integer getIsApproved() {
            return isApproved;
        }

        public void setIsApproved(Integer isApproved) {
            this.isApproved = isApproved;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }
}
