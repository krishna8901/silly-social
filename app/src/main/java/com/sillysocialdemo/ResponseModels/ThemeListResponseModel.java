package com.sillysocialdemo.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ThemeListResponseModel {
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("success")
    @Expose
    private Integer success;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }
    public class Result {

        @SerializedName("contentList")
        @Expose
        private List<Content> contentList = null;

        public List<Content> getContentList() {
            return contentList;
        }

        public void setContentList(List<Content> contentList) {
            this.contentList = contentList;
        }

    }
    public class Content {
        public boolean isPosition() {
            return position;
        }

        public void setPosition(boolean position) {
            this.position = position;
        }

        @SerializedName("indexposition")
        @Expose
        private boolean position;

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("text")
        @Expose
        private String text;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("userId")
        @Expose
        private Integer userId;
        @SerializedName("isApproved")
        @Expose
        private Integer isApproved;
        @SerializedName("createdAt")
        @Expose
        private String createdAt;
        @SerializedName("updatedAt")
        @Expose
        private String updatedAt;
        @SerializedName("usersYoutagCount")
        @Expose
        private Integer usersYoutagCount;
        private boolean isSelected = false;

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getIsApproved() {
            return isApproved;
        }

        public void setIsApproved(Integer isApproved) {
            this.isApproved = isApproved;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Integer getUsersYoutagCount() {
            return usersYoutagCount;
        }

        public void setUsersYoutagCount(Integer usersYoutagCount) {
            this.usersYoutagCount = usersYoutagCount;
        }

    }
}
