package com.sillysocialdemo.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MeetPeopleResponseModel {

    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("success")
    @Expose
    private Integer success;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }


    public class Pagination {

        @SerializedName("page")
        @Expose
        private Integer page;
        @SerializedName("limit")
        @Expose
        private String limit;

        public Integer getPage() {
            return page;
        }

        public void setPage(Integer page) {
            this.page = page;
        }

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

    }

    public class Result {

        @SerializedName("contentList")
        @Expose
        private List<Content> contentList = null;
        @SerializedName("pagination")
        @Expose
        private Pagination pagination;

        public List<Content> getContentList() {
            return contentList;
        }

        public void setContentList(List<Content> contentList) {
            this.contentList = contentList;
        }

        public Pagination getPagination() {
            return pagination;
        }

        public void setPagination(Pagination pagination) {
            this.pagination = pagination;
        }

    }

    public class CommonYoutag {

        @SerializedName("text")
        @Expose
        private String text;
        @SerializedName("id")
        @Expose
        private Integer id;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }
    }

    public class Content {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("textStatus")
        @Expose
        private Object textStatus;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("avatar")
        @Expose
        private Object avatar;
        @SerializedName("openfireId")
        @Expose
        private String openfireId;
        @SerializedName("openfirePassword")
        @Expose
        private String openfirePassword;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("numberOfMatchedYoutags")
        @Expose
        private Integer numberOfMatchedYoutags;
        @SerializedName("numberOfMatchedHashtags")
        @Expose
        private Integer numberOfMatchedHashtags;
        @SerializedName("commonYoutags")
        @Expose
        private List<CommonYoutag> commonYoutags = null;
        @SerializedName("commonHashtags")
        @Expose
        private List<Object> commonHashtags = null;
        @SerializedName("friendshipStatus")
        @Expose
        private Integer friendshipStatus;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getTextStatus() {
            return textStatus;
        }

        public void setTextStatus(Object textStatus) {
            this.textStatus = textStatus;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Object getAvatar() {
            return avatar;
        }

        public void setAvatar(Object avatar) {
            this.avatar = avatar;
        }

        public String getOpenfireId() {
            return openfireId;
        }

        public void setOpenfireId(String openfireId) {
            this.openfireId = openfireId;
        }

        public String getOpenfirePassword() {
            return openfirePassword;
        }

        public void setOpenfirePassword(String openfirePassword) {
            this.openfirePassword = openfirePassword;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public Integer getNumberOfMatchedYoutags() {
            return numberOfMatchedYoutags;
        }

        public void setNumberOfMatchedYoutags(Integer numberOfMatchedYoutags) {
            this.numberOfMatchedYoutags = numberOfMatchedYoutags;
        }

        public Integer getNumberOfMatchedHashtags() {
            return numberOfMatchedHashtags;
        }

        public void setNumberOfMatchedHashtags(Integer numberOfMatchedHashtags) {
            this.numberOfMatchedHashtags = numberOfMatchedHashtags;
        }

        public List<CommonYoutag> getCommonYoutags() {
            return commonYoutags;
        }

        public void setCommonYoutags(List<CommonYoutag> commonYoutags) {
            this.commonYoutags = commonYoutags;
        }

        public List<Object> getCommonHashtags() {
            return commonHashtags;
        }

        public void setCommonHashtags(List<Object> commonHashtags) {
            this.commonHashtags = commonHashtags;
        }

        public Integer getFriendshipStatus() {
            return friendshipStatus;
        }

        public void setFriendshipStatus(Integer friendshipStatus) {
            this.friendshipStatus = friendshipStatus;
        }
    }
}
