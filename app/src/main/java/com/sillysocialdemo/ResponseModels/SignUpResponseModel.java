package com.sillysocialdemo.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SignUpResponseModel {
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("success")
    @Expose
    private Integer success;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }
    public class CreatedAt {

        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("timezone_type")
        @Expose
        private Integer timezoneType;
        @SerializedName("timezone")
        @Expose
        private String timezone;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public Integer getTimezoneType() {
            return timezoneType;
        }

        public void setTimezoneType(Integer timezoneType) {
            this.timezoneType = timezoneType;
        }

        public String getTimezone() {
            return timezone;
        }

        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }
    }
    public class Result {

        @SerializedName("user")
        @Expose
        private User user;
        @SerializedName("accessToken")
        @Expose
        private String accessToken;

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }
    }
    public class User {

        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("createdAt")
        @Expose
        private CreatedAt createdAt;
        @SerializedName("userId")
        @Expose
        private Integer userId;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("images")
        @Expose
        private List<Object> images = null;
        @SerializedName("youtags")
        @Expose
        private List<Object> youtags = null;
        @SerializedName("hashtags")
        @Expose
        private List<Object> hashtags = null;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public CreatedAt getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(CreatedAt createdAt) {
            this.createdAt = createdAt;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public List<Object> getImages() {
            return images;
        }

        public void setImages(List<Object> images) {
            this.images = images;
        }

        public List<Object> getYoutags() {
            return youtags;
        }

        public void setYoutags(List<Object> youtags) {
            this.youtags = youtags;
        }

        public List<Object> getHashtags() {
            return hashtags;
        }

        public void setHashtags(List<Object> hashtags) {
            this.hashtags = hashtags;
        }

    }
}
