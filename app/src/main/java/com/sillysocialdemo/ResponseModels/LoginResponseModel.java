package com.sillysocialdemo.ResponseModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginResponseModel {

    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("success")
    @Expose
    private Integer success;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public class Result {

        @SerializedName("user")
        @Expose
        private User user;
        @SerializedName("accessToken")
        @Expose
        private String accessToken;

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

    }

    public class User {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("textStatus")
        @Expose
        private Object textStatus;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("avatar")
        @Expose
        private Object avatar;
        @SerializedName("openfireId")
        @Expose
        private String openfireId;
        @SerializedName("openfirePassword")
        @Expose
        private String openfirePassword;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("createdAt")
        @Expose
        private String createdAt;
        @SerializedName("coins")
        @Expose
        private Integer coins;
        @SerializedName("isPopupBlocked")
        @Expose
        private Integer isPopupBlocked;
        @SerializedName("images")
        @Expose
        private List<Object> images = null;
        @SerializedName("youtags")
        @Expose
        private List<Youtag> youtags = null;
        @SerializedName("hashtags")
        @Expose
        private List<Object> hashtags = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getTextStatus() {
            return textStatus;
        }

        public void setTextStatus(Object textStatus) {
            this.textStatus = textStatus;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Object getAvatar() {
            return avatar;
        }

        public void setAvatar(Object avatar) {
            this.avatar = avatar;
        }

        public String getOpenfireId() {
            return openfireId;
        }

        public void setOpenfireId(String openfireId) {
            this.openfireId = openfireId;
        }

        public String getOpenfirePassword() {
            return openfirePassword;
        }

        public void setOpenfirePassword(String openfirePassword) {
            this.openfirePassword = openfirePassword;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public Integer getCoins() {
            return coins;
        }

        public void setCoins(Integer coins) {
            this.coins = coins;
        }

        public Integer getIsPopupBlocked() {
            return isPopupBlocked;
        }

        public void setIsPopupBlocked(Integer isPopupBlocked) {
            this.isPopupBlocked = isPopupBlocked;
        }

        public List<Object> getImages() {
            return images;
        }

        public void setImages(List<Object> images) {
            this.images = images;
        }

        public List<Youtag> getYoutags() {
            return youtags;
        }

        public void setYoutags(List<Youtag> youtags) {
            this.youtags = youtags;
        }

        public List<Object> getHashtags() {
            return hashtags;
        }

        public void setHashtags(List<Object> hashtags) {
            this.hashtags = hashtags;
        }

        public class Youtag {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("text")
            @Expose
            private String text;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getText() {
                return text;
            }

            public void setText(String text) {
                this.text = text;
            }

        }
    }
}
