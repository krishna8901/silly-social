package com.sillysocialdemo.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sillysocialdemo.R;
import com.sillysocialdemo.ResponseModels.MeetPeopleResponseModel;
import com.sillysocialdemo.databinding.MeetPeopleRvLayoutBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MeetPeopleAdapter extends RecyclerView.Adapter<MeetPeopleAdapter.VKrishna> {

    List<MeetPeopleResponseModel.Content> list;
    List<MeetPeopleResponseModel.CommonYoutag> listt;
    Context context;

    public MeetPeopleAdapter(List<MeetPeopleResponseModel.Content> list,List<MeetPeopleResponseModel.CommonYoutag> listt, Context context) {
        this.list = list;
        this.listt= listt;
    }

    @NonNull
    @Override
    public MeetPeopleAdapter.VKrishna onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        MeetPeopleRvLayoutBinding binding= DataBindingUtil.inflate(inflater, R.layout.meet_people_rv_layout,parent,false);
        return new VKrishna(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MeetPeopleAdapter.VKrishna holder, int position) {

        MeetPeopleRvLayoutBinding binding= holder.getBinding();
        binding.name.setText(list.get(position).getFirstName()+" "+list.get(position).getLastName());
      //  binding.userImage.setImageURI((Uri) list.get(position).getAvatar());

        List<String> setItem=new ArrayList<>();

        for(int i=0; i<=listt.size()-1; i++){
            setItem.add(listt.get(i).getText().toString());
        }
        ChildAdapter childAdapter = new ChildAdapter(setItem);
        binding.recyclerViewD.setLayoutManager(new LinearLayoutManager(context ,LinearLayoutManager.HORIZONTAL, true));
        binding.recyclerViewD.setAdapter(childAdapter);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VKrishna extends RecyclerView.ViewHolder {
        MeetPeopleRvLayoutBinding binding;
        public VKrishna(@NonNull MeetPeopleRvLayoutBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }
        public MeetPeopleRvLayoutBinding getBinding(){
            return binding;
        }
    }
}
