package com.sillysocialdemo.Adapters;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.sillysocialdemo.R;
import com.sillysocialdemo.databinding.ButtonLayoutBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ChildAdapter extends RecyclerView.Adapter<ChildAdapter.VHolder> {

    ArrayList<String> mList;

    public ChildAdapter(List<String> selectedItem) {
        this.mList = (ArrayList<String>) selectedItem;
    }

    @NonNull
    @Override
    public ChildAdapter.VHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ButtonLayoutBinding binding = DataBindingUtil.inflate(inflater, R.layout.button_layout, parent, false);
        return new ChildAdapter.VHolder(binding);

    }
    @Override
    public void onBindViewHolder(@NonNull ChildAdapter.VHolder holder, int position) {
        ButtonLayoutBinding binding = holder.getBinding();
        binding.selectedText.setText(String.valueOf(mList.get(position)));
        Random r=new Random();
        holder.binding.selectedText.setBackgroundColor(Color.argb(155, r.nextInt(156), r.nextInt(156), r.nextInt(166)));

    }
    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class VHolder extends RecyclerView.ViewHolder {
        ButtonLayoutBinding binding;

        public VHolder(@NonNull ButtonLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public ButtonLayoutBinding getBinding() {
            return binding;
        }
    }
}
