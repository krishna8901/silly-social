package com.sillysocialdemo.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sillysocialdemo.R;

public class CustomSpinnerAdapter extends BaseAdapter {
    Context context;
    int[] indic;
    String[] gender;
    LayoutInflater inflter;

    public CustomSpinnerAdapter(Context applicationContext, int[] indics, String[] genders) {
        this.context = applicationContext;
        this.indic = indics;
        this.gender = genders;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return indic.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_adapter_layout, null);
        ImageView icon =view.findViewById(R.id.indicates_image);
        TextView names =view.findViewById(R.id.tv_gender);
        icon.setImageResource(indic[i]);
        names.setText(gender[i]);
        return view;
    }
}