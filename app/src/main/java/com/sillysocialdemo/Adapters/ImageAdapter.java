package com.sillysocialdemo.Adapters;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.RecyclerView;

import com.sillysocialdemo.R;
import com.sillysocialdemo.databinding.CustomSingleImageBinding;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.VHolder> {
    private ArrayList<Uri> uriArrayList;

    public ImageAdapter(ArrayList<Uri> uriArrayList) {
        this.uriArrayList = uriArrayList;
    }

    @NonNull
    @Override
    public ImageAdapter.VHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        CustomSingleImageBinding  binding =DataBindingUtil.inflate(inflater, R.layout.custom_single_image,parent,false);
        return new ImageAdapter.VHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageAdapter.VHolder holder, @SuppressLint("RecyclerView") int position) {
        CustomSingleImageBinding binding = holder.getBinding();
      //  Picasso.get().load(uriArrayList.get(position).toString()).fit().into(binding.imagePlace);
        binding.imagePlace.setImageURI(uriArrayList.get(position));




        binding.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uriArrayList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position,uriArrayList.size());
            }
        });
    }
    @Override
    public int getItemCount() {
        return uriArrayList.size();
    }

//    public void addItem(int position, Uri viewModel) {
//        uriArrayList.add(position, viewModel);
//        notifyItemInserted(position);
//    }

    public class VHolder extends RecyclerView.ViewHolder {
        CustomSingleImageBinding binding;
        public VHolder(@NonNull CustomSingleImageBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }
        public CustomSingleImageBinding getBinding(){return binding;}
    }
}
