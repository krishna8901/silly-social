package com.sillysocialdemo.Adapters;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.sillysocialdemo.Activity.IntroActivity;
import com.sillysocialdemo.R;

import java.util.Objects;

public class IntroPagerAdapter extends PagerAdapter {

    TypedArray images;
    TypedArray title;
    TypedArray subtitle;
    LayoutInflater layoutInflater;

    public IntroPagerAdapter(IntroActivity context, TypedArray images, TypedArray title, TypedArray subtitle) {
        this.images = images;
        this.title = title;
        this.subtitle = subtitle;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.length();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = layoutInflater.inflate(R.layout.pager_layout, container, false);
        ImageView imageView = view.findViewById(R.id.chat_image);
        TextView textView = view.findViewById(R.id.title1);
        TextView textView1 = view.findViewById(R.id.title2);


        imageView.setBackgroundResource(images.getResourceId(position, 0));
        textView.setText(title.getText(position));
        textView1.setText(subtitle.getText(position));
        Objects.requireNonNull(container).addView(view);
        return view;

    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout) object);
    }
}
