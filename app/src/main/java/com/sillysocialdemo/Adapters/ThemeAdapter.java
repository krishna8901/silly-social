package com.sillysocialdemo.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.orhanobut.hawk.Hawk;
import com.sillysocialdemo.Activity.AddThemeTags;
import com.sillysocialdemo.R;
import com.sillysocialdemo.ResponseModels.ThemeListResponseModel;
import com.sillysocialdemo.databinding.ListLayoutBinding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class ThemeAdapter extends RecyclerView.Adapter<ThemeAdapter.ViewHolder> implements Filterable {

    List<ThemeListResponseModel.Content> list;
    ArrayList<String> tempData = new ArrayList<>();
    Context context;
    ArrayList<String> itemList = new ArrayList<>();
    ArrayList<String> searchedTextList = new ArrayList<>();
    ArrayList<ThemeListResponseModel.Content> backup;

    public ThemeAdapter(List<ThemeListResponseModel.Content> list, Context context) {
        this.list = list;
        this.context = context;
        backup = new ArrayList<>(list);
    }

    @NonNull
    @Override
    public ThemeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ListLayoutBinding binding = DataBindingUtil.inflate(inflater, R.layout.list_layout, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        ThemeListResponseModel.Content data = list.get(position);
        holder.bind(data);
    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence keyword) {
            List<ThemeListResponseModel.Content> filteredData = new ArrayList<>();
            if (keyword.toString().isEmpty())
                filteredData.addAll(backup);
            else {
                for (ThemeListResponseModel.Content obj : backup) {
                    if (obj != null && (obj.getText().toLowerCase().contains(keyword.toString().toLowerCase()))) {
                        filteredData.add(obj);
                        searchedTextList.add(obj.getText());
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredData;
            return results;
        }

        @SuppressLint("NotifyDataSetChanged")
        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            list.clear();
            list.addAll((List<ThemeListResponseModel.Content>) filterResults.values);
            notifyDataSetChanged();
        }
    };

    public class ViewHolder extends RecyclerView.ViewHolder {
        ListLayoutBinding binding;

        public ViewHolder(@NonNull ListLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public ListLayoutBinding getBinding() {
            return binding;
        }

        public void bind(ThemeListResponseModel.Content content) {
            binding.text.setText(content.getText());

            tempData = Hawk.get("text");

            if (tempData != null && tempData.contains(content.getText())) {
                binding.checked.setVisibility(View.VISIBLE);
                content.setSelected(true);
            }

            binding.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!content.isSelected()) {
                        binding.checked.setVisibility(View.VISIBLE);
                        content.setSelected(true);
                        if (tempData != null) {
                            itemList = tempData;
                        }
                        itemList.add(content.getText());
                        Hawk.put("text", itemList);

                    } else {
                        binding.checked.setVisibility(View.GONE);
                        content.setSelected(false);
                        tempData = Hawk.get("text");
                        if (tempData != null) {
                            itemList = tempData;
                        }
                        itemList.remove(content.getText());
                        Hawk.put("text", itemList);
                    }
                    ((AddThemeTags) context).childAdapterCall(itemList);
                }
            });
        }
    }
}

