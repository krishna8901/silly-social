package com.sillysocialdemo.Adapters;
import android.content.Context;
import android.graphics.Color;
import android.net.ParseException;
import android.net.Uri;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.sillysocialdemo.R;
import com.sillysocialdemo.ResponseModels.FeedListResponseModel;
import com.sillysocialdemo.databinding.FeedItemLayoutBinding;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class FeedListAdapter extends RecyclerView.Adapter<FeedListAdapter.VHolder> {

   List<FeedListResponseModel.Content> responseList;
   Context context;
   boolean click=true;

    public FeedListAdapter(List<FeedListResponseModel.Content> responseItem, Context context) {

        this.responseList= responseItem;
        this.context=context;
    }

    @NonNull
    @Override
    public VHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        FeedItemLayoutBinding binding= DataBindingUtil.inflate(inflater, R.layout.feed_item_layout,parent,false);
        return new VHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull VHolder holder, int position) {
        FeedItemLayoutBinding binding=holder.getBinding();
        binding.name.setText((CharSequence) responseList.get(position).getFirstName()+" "+responseList.get(position).getLastName());
        binding.descriptions1.setText(responseList.get(position).getComment().getComment());

        Random r=new Random();
        holder.binding.descriptions2.setBackgroundColor(Color.argb(155, r.nextInt(156), r.nextInt(156), r.nextInt(166)));
        binding.descriptions2.setText(responseList.get(position).getFeedYoutag().get(0).getTags().getText());

        if(((responseList.get(position).getAvatar()!=null))){
            binding.userImage.setImageURI(Uri.parse(responseList.get(position).getAvatar()));
        }

        String timeAgo=calculateTimeAgo(responseList.get(position).getCreatedAt());
        binding.postDate.setText(timeAgo);


    String imageUrl= String.valueOf(responseList.get(position).getMedia());

        if(imageUrl!=null && !imageUrl.isEmpty()) {
            binding.recyclerViewPostMedia.setVisibility(View.VISIBLE);
            MediaAdapter adapter = new MediaAdapter(Collections.singletonList(imageUrl));
            binding.recyclerViewPostMedia.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true));
            binding.recyclerViewPostMedia.setAdapter(adapter);

        }

        binding.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(click){
                    binding.like.setColorFilter(ContextCompat.getColor(context, R.color.red));
                    binding.likeCount.setText("1");
                    click=false;
                }else {
                    binding.like.setColorFilter(ContextCompat.getColor(context, R.color.like_color));
                    binding.likeCount.setText("0");
                    click=true;
                }
            }
        });
    }

    private String calculateTimeAgo(String createdAt) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd HH:mm:ss");
        try {
            long time = sdf.parse(createdAt).getTime();
            long now = System.currentTimeMillis();
            CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
            return ago+"";
        } catch (ParseException | java.text.ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public int getItemCount() {
        return responseList.size();
    }

    public class VHolder extends RecyclerView.ViewHolder {

        FeedItemLayoutBinding binding;
        public VHolder(@NonNull FeedItemLayoutBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }
        public FeedItemLayoutBinding getBinding(){return binding;}
    }
}
