package com.sillysocialdemo.Adapters;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.sillysocialdemo.R;
import com.sillysocialdemo.ResponseModels.FeedListResponseModel;
import com.sillysocialdemo.databinding.MediaLayoutBinding;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MediaAdapter extends RecyclerView.Adapter<MediaAdapter.VHolder> {
   // List<Object> media;
    List<Object> media;

    public MediaAdapter(List<Object> media) {
        this.media = media;
    }

    @NonNull
    @Override
    public VHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        MediaLayoutBinding binding= DataBindingUtil.inflate(inflater, R.layout.media_layout,parent,false);
        return new VHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull VHolder holder, int position) {
        MediaLayoutBinding binding=holder.getBinding();
        Picasso.get().load(media.get(position).toString()).fit().into(binding.images);
        binding.images.setImageURI(Uri.parse(media.get(position).toString()));

    }

    @Override
    public int getItemCount() {
        return media.size();
    }

    public class VHolder extends RecyclerView.ViewHolder {
        MediaLayoutBinding binding;
        public VHolder(@NonNull MediaLayoutBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }
        public MediaLayoutBinding getBinding(){return binding;}
    }
}
