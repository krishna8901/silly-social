package com.sillysocialdemo.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;

import com.sillysocialdemo.R;
import com.sillysocialdemo.databinding.ActivityHamburgerMenuBinding;

public class HamburgerMenuActivity extends AppCompatActivity {

    ActivityHamburgerMenuBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hamburger_menu);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_hamburger_menu);
        binding.backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        binding.filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }
}