package com.sillysocialdemo.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.orhanobut.hawk.Hawk;
import com.sillysocialdemo.SillySocialServices.ApiEndPoint;
import com.sillysocialdemo.RequestModels.CreateProfileRequestModel;
import com.sillysocialdemo.ResponseModels.CreateProfileResponseModel;
import com.sillysocialdemo.Adapters.CustomSpinnerAdapter;
import com.sillysocialdemo.R;
import com.sillysocialdemo.SillySocialServices.ServiceGenerator;
import com.sillysocialdemo.SillySocialServices.ValidationService;
import com.sillysocialdemo.databinding.ActivityCompleteProfileBinding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompleteProfile extends AppCompatActivity {

    ActivityCompleteProfileBinding binding;
    String genderSelected;
    public static final String accessToken = "AToken";
    public static final String responseBody= "resBody";
    public static final String userid ="userId";
    public static final String done ="done";
    String profileImage;

    String[] gender = {"Male", "Female", "Other", "Don't wish to disclose"};
    int[] indicats = {R.drawable.ic_signs_08, R.drawable.ic_female, R.drawable.ic_other, R.drawable.ic_none};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_complete_profile);
        Hawk.init(this).build();

        CustomSpinnerAdapter customAdapter = new CustomSpinnerAdapter(getApplicationContext(), indicats, gender);
        binding.spinner.setAdapter(customAdapter);
        binding.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                genderSelected = gender[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.userId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagePicker.with(CompleteProfile.this)
                        .crop()                    //Crop image(Optional), Check Customization for more option
                        .compress(100)            //Final image size will be less than 1 MB(Optional)
                        .maxResultSize(1080, 1080)//Final image resolution will be less than 1080 x 1080(Optional)
                        .start();
            }
        });

        binding.createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ValidationService.emptyEmail(binding.etFirstName.getText().toString())) {
                    Toast.makeText(CompleteProfile.this, "First name cannot be left blank", Toast.LENGTH_SHORT).show();
                } else if (ValidationService.emptyPassword(binding.etLastName.getText().toString())) {
                    Toast.makeText(CompleteProfile.this, "Last name cannot be left blank", Toast.LENGTH_SHORT).show();
                } else {
                    inputData();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri uri = data.getData();
        profileImage = uri.getPath();
        binding.imagePickerBackground.setImageURI(uri);
    }

    public void inputData() {

        List<Integer> mlist = new ArrayList<>();
        mlist.add(1006);
        List<String> imagelist = new ArrayList<>();
        imagelist.add(profileImage);
        ApiEndPoint client = ServiceGenerator.createService(ApiEndPoint.class);

        CreateProfileRequestModel requestModel = new CreateProfileRequestModel();
        requestModel.setImages(Collections.singletonList(" "));
        requestModel.setFirstName(binding.etFirstName.getText().toString());
        requestModel.setLastName(binding.etLastName.getText().toString());
        requestModel.setGender(genderSelected);
        requestModel.setAcceptTerm(1);
        requestModel.setEmail(getIntent().getStringExtra("email"));
        requestModel.setYoutags(mlist);
        requestModel.setImages(imagelist);
        String accessTkn = Hawk.get(accessToken);

        Call<CreateProfileResponseModel> responseModelCall = client.getCreateResponse(accessTkn, requestModel);
        responseModelCall.enqueue(new Callback<CreateProfileResponseModel>() {
            @Override
            public void onResponse(Call<CreateProfileResponseModel> call, Response<CreateProfileResponseModel> response) {
                response.isSuccessful();
                Hawk.init(getApplication()).build();
                Intent intent = new Intent(CompleteProfile.this, AddMoreDetailsActivity.class);
                Hawk.put(responseBody,response.body());
                Hawk.put(userid,response.body().getResult().getUser().getId());
                Hawk.delete("text");
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<CreateProfileResponseModel> call, Throwable t) {
                Toast.makeText(CompleteProfile.this, "something went wrong", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }
}