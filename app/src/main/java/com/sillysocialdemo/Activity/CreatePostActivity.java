package com.sillysocialdemo.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.sillysocialdemo.Adapters.ImageAdapter;
import com.sillysocialdemo.R;
import com.sillysocialdemo.databinding.ActivityCreatePostBinding;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class CreatePostActivity extends AppCompatActivity {

    ActivityCreatePostBinding binding;
    String picImage;
    ArrayList<Uri> images = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_post);

        binding.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        binding.addTheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(CreatePostActivity.this,AddThemeTags.class);
                startActivity(intent);
            }
        });

        binding.addPhotoVdo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }

        });
    }
    private void showDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.camera_alart_dialog_box);

        LinearLayout gallery = dialog.findViewById(R.id.gallery);
        LinearLayout camera = dialog.findViewById(R.id.camera);
        LinearLayout videorecording=dialog.findViewById(R.id.video);


        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkAndRequestPermissions()){
                    takePictureFromCamera();
                    dialog.dismiss();
                }
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePictureFromGallery();
                dialog.dismiss();
            }
        });

        videorecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recordVideos();
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.BOTTOM);

    }
    private void takePictureFromGallery(){
        //Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
       // pickPhoto.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"), 1);
       // startActivityForResult(intent, 1);
    }

    private void takePictureFromCamera(){
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePicture.resolveActivity(getPackageManager()) != null){
            startActivityForResult(takePicture, 2);
        }
    }
    private void recordVideos(){
        Intent recordVdo = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if(recordVdo.resolveActivity(getPackageManager()) !=null){
            startActivityForResult(recordVdo,3);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        ImageAdapter imageAdapter = new ImageAdapter(images );

        binding.imageRecyclerview.setLayoutManager(new GridLayoutManager(CreatePostActivity.this, 3));
        binding.imageRecyclerview.setAdapter(imageAdapter);

        switch (requestCode)
        {
            case 1:
                if(resultCode == RESULT_OK){

                    if(data.getClipData() !=null){
                        int count =data.getClipData().getItemCount();
                        for (int i=0; i<count; i++){
                            Uri imageUri=data.getClipData().getItemAt(i).getUri();
                            images.add(imageUri);
                        }
                    }else {
                        Uri selectedImageUri = data.getData();
                        picImage = selectedImageUri.getPath();
                        images.add(selectedImageUri);
                    }
//                    imageAdapter.addItem(images.size(),selectedImageUri);

                }
//                else {
//                    VideoView videoView=new VideoView(this);
//                    videoView.setVideoURI(data.getData());
//                    videoView.start();
//                }
                break;

            case 2:
                if(resultCode == RESULT_OK){
                    Bundle bundle = data.getExtras();
                    Bitmap bitmapImage = (Bitmap) bundle.get("data");
                    //images.add(getImageUri(getApplicationContext(),bitmapImage));
                    ByteArrayOutputStream bytes =new ByteArrayOutputStream();
                    bitmapImage.compress(Bitmap.CompressFormat.JPEG,100,bytes);
                    String path= MediaStore.Images.Media.insertImage(getApplicationContext().getContentResolver(),bitmapImage,"val",null);
                    Uri uri=Uri.parse(path);
                    images.add(uri);
                   // binding.imgView.setImageURI(uri);
                 //   binding.imgView.setImageBitmap(bitmapImage);


                }
                break;
//                if (requestCode == RESULT_OK){
//                    recordVideo
//                    Bundle bundle= data.getExtras();
//
//                }
        }
    }

    private boolean checkAndRequestPermissions(){
        if(Build.VERSION.SDK_INT >= 23){
            int cameraPermission = ActivityCompat.checkSelfPermission(CreatePostActivity.this, Manifest.permission.CAMERA);
            if(cameraPermission == PackageManager.PERMISSION_DENIED){
                ActivityCompat.requestPermissions(CreatePostActivity.this, new String[]{Manifest.permission.CAMERA}, 20);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 20 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            takePictureFromCamera();
        }
        else
            Toast.makeText(CreatePostActivity.this, "Permission not Granted", Toast.LENGTH_SHORT).show();
    }
}