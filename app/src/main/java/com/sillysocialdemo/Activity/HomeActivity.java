package com.sillysocialdemo.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.google.android.material.tabs.TabLayout;
import com.sillysocialdemo.R;
import com.sillysocialdemo.MeetPeopleFragment;
import com.sillysocialdemo.databinding.ActivityHomeBinding;

public class HomeActivity extends AppCompatActivity {

    ActivityHomeBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);

        binding.tablayout.addTab(binding.tablayout.newTab().setText("FEED"));
        binding.tablayout.addTab(binding.tablayout.newTab().setText("MEET PEOPLE"));

        binding.menuIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, HamburgerMenuActivity.class);
                startActivity(intent);
            }
        });
        binding.floatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(HomeActivity.this,CreatePostActivity.class);
                startActivity(intent);
            }
        });

        binding.bottomNavView.setOnNavigationItemSelectedListener(item -> {

            switch (item.getItemId()) {

                case R.id.search:
                    Intent inn = new Intent(HomeActivity.this, ChatActivity.class);
                    startActivity(inn);
                    break;

                case R.id.chat:
                    Intent intnt = new Intent(HomeActivity.this, ChatActivity.class);
                    startActivity(intnt);
                    break;

                case R.id.profile:
                    Intent intet = new Intent(HomeActivity.this, ViewProfileActivity.class);
                    startActivity(intet);
                    break;

            }
            return false;
        });

        binding.viewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                if (position == 0) {
                    return new FeedListFragment();
                } else {
                    return new MeetPeopleFragment();
                }
            }

            @Override
            public int getCount() {
                return 2;
            }
        });
        binding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tablayout));
        binding.tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                binding.viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}