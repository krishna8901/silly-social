package com.sillysocialdemo.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import com.orhanobut.hawk.Hawk;
import com.sillysocialdemo.R;
import com.sillysocialdemo.ResponseModels.CreateProfileResponseModel;
import com.sillysocialdemo.ResponseModels.LoginResponseModel;
import com.sillysocialdemo.ResponseModels.SignUpResponseModel;
import com.sillysocialdemo.databinding.ActivitySplashBinding;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ActivitySplashBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        binding.image.setBackgroundResource(android.R.color.transparent);
        Hawk.init(this).build();

        SignUpResponseModel response = Hawk.get("ResponseB", null);
        CreateProfileResponseModel responseModel = Hawk.get("resBody", null);

        String addMore = Hawk.get("done", "b");
        String intro =Hawk.get("intro","n");

        LoginResponseModel loginRes = Hawk.get("loginDetails", null);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (response == null && loginRes == null) {
                    if(!intro.equals("12")) {
                        Intent intent = new Intent(SplashActivity.this, IntroActivity.class);
                        startActivity(intent);
                        finish();
                    }else{
                        Intent intent = new Intent(SplashActivity.this, SignIn.class);
                        startActivity(intent);
                        finish();
                    }
                } else if (loginRes != null) {
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                } else if (responseModel == null) {
                    Intent intent = new Intent(SplashActivity.this, CompleteProfile.class);
                    startActivity(intent);
                    finish();
                } else if (!addMore.equals("a")) {
                    Intent intent = new Intent(SplashActivity.this, CompleteProfile.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, 2000);
    }
}