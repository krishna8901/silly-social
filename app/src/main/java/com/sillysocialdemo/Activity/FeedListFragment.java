package com.sillysocialdemo.Activity;

import android.content.Context;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.orhanobut.hawk.Hawk;
import com.sillysocialdemo.Adapters.FeedListAdapter;
import com.sillysocialdemo.Adapters.MeetPeopleAdapter;
import com.sillysocialdemo.R;
import com.sillysocialdemo.RequestModels.FeedListRequestModel;
import com.sillysocialdemo.ResponseModels.FeedListResponseModel;
import com.sillysocialdemo.SillySocialServices.ApiEndPoint;
import com.sillysocialdemo.SillySocialServices.ServiceGenerator;
import com.sillysocialdemo.databinding.FragmentFeedListBinding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedListFragment extends Fragment {
    FragmentFeedListBinding binding;

    public static final String accessToken = "AToken";
    public static final String userid ="userId";
    List<FeedListResponseModel.Content> responseName;
    List<FeedListResponseModel.Content> comments;

    int page=1;
    int limit=30;
    int filterType=0;
  //  int userId=0;

    public FeedListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        binding= DataBindingUtil.inflate(inflater,R.layout.fragment_feed_list,container,false);

        feedListCall();
        return binding.getRoot();
    }
    public void feedListCall(){

        Hawk.init(getActivity()).build();
        ApiEndPoint client = ServiceGenerator.createService(ApiEndPoint.class);
        String accessTkn = Hawk.get(accessToken);
        Integer user =Hawk.get(userid);

        FeedListRequestModel requestModel=new FeedListRequestModel();
        requestModel.setPage(page);
        requestModel.setLimit(limit);
        requestModel.setFilterType(filterType);
        requestModel.setUserId(user);

        Call<FeedListResponseModel> responseModelCall=client.getFeedResponse(accessTkn,requestModel);
        responseModelCall.enqueue(new Callback<FeedListResponseModel>() {
            @Override
            public void onResponse(Call<FeedListResponseModel> call, Response<FeedListResponseModel> response) {
                if(response.isSuccessful()){
                //responseItem = response.body().getResult().getContentList().get(19).getFirstName();
               responseName = response.body().getResult().getContentList();
               comments =response.body().getResult().getContentList();
               response.body().getResult().getContentList().get(1).getImage().toString();


                    FeedListAdapter adapter = new FeedListAdapter(responseName,requireContext());
                    binding.feedListRecyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
                    binding.feedListRecyclerview.setAdapter(adapter);

                }
            }

            @Override
            public void onFailure(Call<FeedListResponseModel> call, Throwable t) {

            }
        });

    }
}