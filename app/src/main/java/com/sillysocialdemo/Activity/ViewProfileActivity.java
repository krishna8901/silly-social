package com.sillysocialdemo.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.orhanobut.hawk.Hawk;
import com.sillysocialdemo.R;
import com.sillysocialdemo.RequestModels.ViewProfileRequestModel;
import com.sillysocialdemo.ResponseModels.ViewProfileResponseModel;
import com.sillysocialdemo.SillySocialServices.ApiEndPoint;
import com.sillysocialdemo.SillySocialServices.ServiceGenerator;
import com.sillysocialdemo.databinding.ViewProfileBinding;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewProfileActivity extends AppCompatActivity {

    ViewProfileBinding binding;
    public static final String accessToken = "AToken";
    public static final String userId = "userID";
    String profileImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_profile);
        binding = DataBindingUtil.setContentView(this, R.layout.view_profile);

        viewProfile();
        binding.backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        binding.settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViewProfileActivity.this, SettingsActivity.class);
                startActivity(intent);
                finish();

            }
        });

    }

    public void viewProfile() {
        ApiEndPoint client = ServiceGenerator.createService(ApiEndPoint.class);
        Integer userid = Hawk.get(userId);
        String accessTkn = Hawk.get(accessToken);

        ViewProfileRequestModel requestModel = new ViewProfileRequestModel();
        requestModel.setUserId(userid);

        Call<ViewProfileResponseModel> responseModelCall = client.getViewProfile(accessTkn, requestModel);
        responseModelCall.enqueue(new Callback<ViewProfileResponseModel>() {
            @Override
            public void onResponse(Call<ViewProfileResponseModel> call, Response<ViewProfileResponseModel> response) {
                if (response.isSuccessful()) {
                    response.body().getResult();
                    binding.perSonNm.setText(response.body().getResult().getUser().getFirstName() + " " + response.body().getResult().getUser().getLastName());
                    binding.imagePickerBackground.setImageURI(Uri.parse(response.body().getResult().getUser().getImages().get(0).toString()));


                    Uri url=Uri.parse(response.body().getResult().getUser().getImages().get(0).toString());

                    Log.e("xyz",url.toString());
                }

            }

            @Override
            public void onFailure(Call<ViewProfileResponseModel> call, Throwable t) {

            }
        });

        binding.userId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagePicker.with(ViewProfileActivity.this)
                        .crop()                    //Crop image(Optional), Check Customization for more option
                        .compress(100)            //Final image size will be less than 1 MB(Optional)
                        .maxResultSize(1080, 1080)//Final image resolution will be less than 1080 x 1080(Optional)
                        .start();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri uri = data.getData();
        profileImage = uri.getPath();
        binding.imagePickerBackground.setImageURI(uri);
    }
}