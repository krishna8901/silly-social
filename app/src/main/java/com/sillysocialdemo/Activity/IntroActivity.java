package com.sillysocialdemo.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.orhanobut.hawk.Hawk;
import com.sillysocialdemo.Adapters.IntroPagerAdapter;
import com.sillysocialdemo.R;
import com.sillysocialdemo.databinding.ActivityIntroBinding;

public class IntroActivity extends AppCompatActivity {

    private int dostscount;
    private ImageView[] dots;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        ActivityIntroBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_intro);

        Hawk.init(this).build();

        TypedArray images = getResources().obtainTypedArray(R.array.image_intro);
        TypedArray title = getResources().obtainTypedArray(R.array.title);
        TypedArray sub_title = getResources().obtainTypedArray(R.array.sub_title);

        IntroPagerAdapter introAdapter = new IntroPagerAdapter(this, images, title, sub_title);
        binding.vpager.setAdapter(introAdapter);

        dostscount = introAdapter.getCount();
        dots = new ImageView[dostscount];

        binding.skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(IntroActivity.this, SignIn.class);
                startActivity(intent);
                Hawk.put("intro", "12");
                finish();
            }
        });
        binding.botton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(IntroActivity.this, SignIn.class);
                startActivity(intent);
                Hawk.put("intro", "12");
                finish();
            }
        });

        for (int i = 0; i < dostscount; i++) {

            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.non_active));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(18, 0, 18, 0);

            binding.sliderDots.addView(dots[i], params);
        }
        dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

        binding.vpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < dostscount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.non_active));
                    if (position == 1) {
                        binding.frame.setVisibility(View.INVISIBLE);
                        binding.botton.setVisibility(View.VISIBLE);
                    } else {
                        binding.frame.setVisibility(View.VISIBLE);
                        binding.botton.setVisibility(View.INVISIBLE);
                    }
                }
                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}