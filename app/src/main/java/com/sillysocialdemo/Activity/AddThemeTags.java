package com.sillysocialdemo.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;
import com.sillysocialdemo.Adapters.ChildAdapter;
import com.sillysocialdemo.Adapters.ThemeAdapter;
import com.sillysocialdemo.R;
import com.sillysocialdemo.ResponseModels.ThemeListResponseModel;
import com.sillysocialdemo.SillySocialServices.ApiEndPoint;
import com.sillysocialdemo.SillySocialServices.ServiceGenerator;
import com.sillysocialdemo.databinding.ActivityAddThemeTagsBinding;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddThemeTags extends AppCompatActivity {

    ActivityAddThemeTagsBinding binding;
    public static final String accessToken = "AToken";
    List<ThemeListResponseModel.Content> themeTag;
    ChildAdapter childAdapter;
    ThemeAdapter adapter;
    int page = 1;
    int limit = 10;
    ArrayList<String> tempData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_theme_tags);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_theme_tags);

        binding.backArrow.setOnClickListener(view -> {
            onBackPressed();
            finish();
        });
        binding.newTheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(AddThemeTags.this, "Comming Soon!", Toast.LENGTH_SHORT).show();
            }
        });

        tagList(page, limit);
        tempData = Hawk.get("text");
        if (tempData != null) {
            childAdapter = new ChildAdapter(tempData);
            binding.recyclerView.setVisibility(View.VISIBLE);
        } else {
            ArrayList<String> tempDat = new ArrayList<>();
            childAdapter = new ChildAdapter(tempDat);
        }
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(AddThemeTags.this, LinearLayoutManager.HORIZONTAL, false));
        binding.recyclerView.setAdapter(childAdapter);

    }

    public void tagList(int page, int limit) {
        ApiEndPoint client = ServiceGenerator.createService(ApiEndPoint.class);
        Hawk.init(this).build();
        String accesTokrnn = Hawk.get(accessToken);

        Call<ThemeListResponseModel> responseModelCall = client.getThemlist(accesTokrnn, limit, page);
        responseModelCall.enqueue(new Callback<ThemeListResponseModel>() {
            @Override
            public void onResponse(Call<ThemeListResponseModel> call, @NonNull Response<ThemeListResponseModel> response) {
                if (response.isSuccessful()) {
                    themeTag = response.body().getResult().getContentList();
                    response.body().getResult().getContentList().get(1).getText().toString();

                    adapter = new ThemeAdapter(themeTag, AddThemeTags.this);
                    binding.themeList.setLayoutManager(new LinearLayoutManager(AddThemeTags.this));
                    binding.themeList.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<ThemeListResponseModel> call, Throwable t) {

            }
        });

        binding.done.setOnClickListener(view -> {
            onBackPressed();
            finish();

        });

        binding.searchBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                try {
                    adapter.getFilter().filter(newText);
                    return true;

                } catch (NullPointerException e) {
                }
                return false;
            }
        });
    }

    public void childAdapterCall(ArrayList<String> itemList) {

        tempData = Hawk.get("text");
        if (tempData != null) {
            childAdapter = new ChildAdapter(tempData);
            binding.recyclerView.setVisibility(View.VISIBLE);
        } else {
            ArrayList<String> tempDat = new ArrayList<>();
            childAdapter = new ChildAdapter(tempDat);
        }
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(AddThemeTags.this, LinearLayoutManager.HORIZONTAL, false));

        binding.recyclerView.setAdapter(childAdapter);
    }
}