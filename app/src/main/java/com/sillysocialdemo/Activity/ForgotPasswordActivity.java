package com.sillysocialdemo.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.sillysocialdemo.SillySocialServices.ApiEndPoint;
import com.sillysocialdemo.RequestModels.ForgotPasswordRequestModel;
import com.sillysocialdemo.ResponseModels.ForgotPasswordResponseModel;
import com.sillysocialdemo.R;
import com.sillysocialdemo.SillySocialServices.ServiceGenerator;
import com.sillysocialdemo.SillySocialServices.ValidationService;
import com.sillysocialdemo.databinding.ActivityForgotPasswordBinding;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity {
    ActivityForgotPasswordBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        binding.backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        binding.tvReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ValidationService.emptyEmail(binding.etEmailAddress.getText().toString())) {
                    Toast.makeText(ForgotPasswordActivity.this, "Email cannot be left blank", Toast.LENGTH_SHORT).show();
                } else {
                    inputEmail();
                }
            }
        });
    }

    public void inputEmail() {
        ApiEndPoint client = ServiceGenerator.createService(ApiEndPoint.class);
        ForgotPasswordRequestModel requestModell = new ForgotPasswordRequestModel();
        requestModell.setEmail(binding.etEmailAddress.getText().toString());

        Call<ForgotPasswordResponseModel> responseModelCall = client.getForgotPassword(requestModell);
        responseModelCall.enqueue(new Callback<ForgotPasswordResponseModel>() {
            @Override
            public void onResponse(Call<ForgotPasswordResponseModel> call, Response<ForgotPasswordResponseModel> response) {
                response.isSuccessful();
                if (response.body().getMessage().equals("Email doesn't exist")) {
                    Toast.makeText(ForgotPasswordActivity.this, "Please enter valid email", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ForgotPasswordActivity.this, "A new password has been send", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordResponseModel> call, Throwable t) {

            }
        });
    }
}