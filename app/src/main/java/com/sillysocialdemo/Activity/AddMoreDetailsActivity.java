package com.sillysocialdemo.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.orhanobut.hawk.Hawk;
import com.sillysocialdemo.Adapters.ChildAdapter;
import com.sillysocialdemo.Adapters.ImageAdapter;
import com.sillysocialdemo.R;
import com.sillysocialdemo.databinding.ActivityAddMoreDetailsBinding;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class AddMoreDetailsActivity extends AppCompatActivity {

    ActivityAddMoreDetailsBinding binding;
    String picImage;
    ArrayList<Uri> images = new ArrayList<>();
    static ArrayList<String> Selectedtag = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_more_details);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_more_details);

        binding.backArrow.setOnClickListener(view -> {
            onBackPressed();
            finish();
        });
        binding.done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Selectedtag == null) {
                    Toast.makeText(AddMoreDetailsActivity.this, "Please Select a ThemeTag !", Toast.LENGTH_SHORT).show();
                } else {
                    if (Selectedtag.isEmpty()) {
                        Toast.makeText(AddMoreDetailsActivity.this, "Please Select a ThemeTag !", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(AddMoreDetailsActivity.this, HomeActivity.class);
                        Hawk.init(getApplication()).build();
                        Hawk.put("done", "a");
                        startActivity(intent);
                        finishAffinity();
                    }
                }
            }
        });

        binding.cardView4.setOnClickListener(view -> {
            Intent intent = new Intent(AddMoreDetailsActivity.this, AddThemeTags.class);
            startActivity(intent);
        });

        binding.addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagePicker.with(AddMoreDetailsActivity.this)
                        .crop()//Crop image(Optional), Check Customization for more option
                        .compress(100)            //Final image size will be less than 1 MB(Optional)
                        .maxResultSize(1080, 1080)//Final image resolution will be less than 1080 x 1080(Optional)
                        .start();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri uri = Objects.requireNonNull(data).getData();
        picImage = uri.getPath();
        images.add(Uri.parse(picImage));
//      binding.imagePickerBackground.setImageURI(uri);
        ImageAdapter imageAdapter = new ImageAdapter(images);
        binding.rvImage.setLayoutManager(new GridLayoutManager(AddMoreDetailsActivity.this, 3));
        binding.rvImage.setAdapter(imageAdapter);
    }

    @Override
    protected void onStart() {
        Hawk.init(this).build();
        Selectedtag = Hawk.get("text");
        if (Selectedtag != null) {
            ChildAdapter childAdapter = new ChildAdapter(Selectedtag);
            binding.rV.setLayoutManager(new LinearLayoutManager(AddMoreDetailsActivity.this, LinearLayoutManager.HORIZONTAL, false));
            binding.rV.setAdapter(childAdapter);
        }
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}