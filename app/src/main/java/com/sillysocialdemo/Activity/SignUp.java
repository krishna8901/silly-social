package com.sillysocialdemo.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;
import com.sillysocialdemo.RequestModels.SignUpRequestModel;
import com.sillysocialdemo.ResponseModels.SignUpResponseModel;
import com.sillysocialdemo.SillySocialServices.ApiEndPoint;
import com.sillysocialdemo.R;
import com.sillysocialdemo.SillySocialServices.ServiceGenerator;
import com.sillysocialdemo.SillySocialServices.ValidationService;
import com.sillysocialdemo.databinding.ActivitySignUpBinding;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUp extends AppCompatActivity {

    ActivitySignUpBinding binding;
    public static final String accessToken = "AToken";
    public static final String responseB = "ResponseB";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);

        binding.alreadyMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });
        binding.tC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(SignUp.this, "Comming Soon..", Toast.LENGTH_SHORT).show();
            }
        });

        binding.tvSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ValidationService.emptyEmail(binding.etEmail.getText().toString())) {
                    Toast.makeText(SignUp.this, "Email cannot be left blank", Toast.LENGTH_SHORT).show();
                } else if (!ValidationService.validEmail(binding.etEmail.getText().toString())) {
                    Toast.makeText(SignUp.this, "Please enter a valid email", Toast.LENGTH_SHORT).show();

                } else if (ValidationService.emptyPassword(binding.etPassword.getText().toString())) {
                    Toast.makeText(SignUp.this, "Password cannot be left blank", Toast.LENGTH_SHORT).show();
                } else if (ValidationService.passLength(binding.etPassword.getText().toString())) {
                    Toast.makeText(SignUp.this, "Password Should not be less then 6 characters", Toast.LENGTH_SHORT).show();

                } else if (!ValidationService.validPassword(binding.etPassword.getText().toString())) {
                    Toast.makeText(SignUp.this, "Please enter valid password", Toast.LENGTH_SHORT).show();
                } else if (ValidationService.emptyPassword(binding.etConfirmPassword.getText().toString())) {
                    Toast.makeText(SignUp.this, "enter confirm pass", Toast.LENGTH_SHORT).show();

                } else if (!binding.etPassword.getText().toString().equals(binding.etConfirmPassword.getText().toString())) {
                    Toast.makeText(SignUp.this, "Pass didnot match", Toast.LENGTH_SHORT).show();
                } else {
                    inputdata();
                }
            }
        });
    }

    public void inputdata() {
        ApiEndPoint client = ServiceGenerator.createService(ApiEndPoint.class);

        String acceptterm = "1";
        String deviceToken = "d19_ozzhwrw:apa91beechqphvz1mubwwynpk5ceid9nepabvkedhtojogkxthgnd563zd6ncnqogwsd1wl8gs4aj4hh6yq93a_0lfz0neolue7nthr5crltqcuejisrn_pjst5cusjvbiyajbtgk9cq";
        String deviceType = "2";

        SignUpRequestModel signUpRequestMode = new SignUpRequestModel();
        signUpRequestMode.setEmail(binding.etEmail.getText().toString());
        signUpRequestMode.setPassword(binding.etPassword.getText().toString());
        signUpRequestMode.setAcceptterm(acceptterm);
        signUpRequestMode.setDeviceToken(deviceToken);
        signUpRequestMode.setDeviceType(deviceType);

        Call<SignUpResponseModel> response = client.getSignUpResponse(signUpRequestMode);
        response.enqueue(new Callback<SignUpResponseModel>() {
            @Override
            public void onResponse(Call<SignUpResponseModel> call, Response<SignUpResponseModel> response) {
                response.isSuccessful();

                Hawk.init(getApplicationContext()).build();
                Hawk.put(accessToken, response.body().getResult().getAccessToken());
                Hawk.put(responseB, response.body());
                checkStatus(response.body());
            }

            @Override
            public void onFailure(Call<SignUpResponseModel> call, Throwable t) {

            }
        });
    }

    private void checkStatus(SignUpResponseModel body) {

        if (body.getMessage().equals("Email id already exists")) {
            Toast.makeText(this, body.getMessage().toString(), Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(SignUp.this, CompleteProfile.class);
            intent.putExtra("email", binding.etEmail.getText().toString());
            startActivity(intent);
            finishAffinity();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}