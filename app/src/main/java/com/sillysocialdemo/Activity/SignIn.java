package com.sillysocialdemo.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;
import com.sillysocialdemo.SillySocialServices.ApiEndPoint;
import com.sillysocialdemo.RequestModels.LoginRequestModel;
import com.sillysocialdemo.ResponseModels.LoginResponseModel;
import com.sillysocialdemo.R;
import com.sillysocialdemo.SillySocialServices.ServiceGenerator;
import com.sillysocialdemo.SillySocialServices.ValidationService;
import com.sillysocialdemo.databinding.SignInLayoutBinding;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignIn extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private static final String TAG = SignIn.class.getCanonicalName();
    public static final String accessToken = "AToken";
    public static final String userid ="userId";
    SignInLayoutBinding binding;
    String mEmailId;
    String mPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in_layout);
        binding = DataBindingUtil.setContentView(this, R.layout.sign_in_layout);

        binding.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignIn.this, AddMoreDetailsActivity.class);
                Toast.makeText(SignIn.this, "clickdd", Toast.LENGTH_SHORT).show();
                startActivity(intent);
                finish();
            }
        });

        binding.notMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignIn.this, SignUp.class);
                startActivity(intent);
            }
        });
        binding.forgotPd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignIn.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        binding.tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ValidationService.emptyEmail(binding.etEmail.getText().toString())) {
                    Toast.makeText(SignIn.this, "Email cannot be left blank", Toast.LENGTH_SHORT).show();
                } else if (!ValidationService.validEmail(binding.etEmail.getText().toString())) {
                    Toast.makeText(SignIn.this, "Please enter a valid email", Toast.LENGTH_SHORT).show();

                } else if (ValidationService.emptyPassword(binding.etPassword.getText().toString())) {
                    Toast.makeText(SignIn.this, "Password cannot be left blank", Toast.LENGTH_SHORT).show();

                } else if (!ValidationService.validPassword(binding.etPassword.getText().toString())) {
                    Toast.makeText(SignIn.this, "Please enter valid password", Toast.LENGTH_SHORT).show();
                } else {

                    mEmailId = binding.etEmail.getText().toString();
                    mPassword = binding.etPassword.getText().toString();

                    signIn();
                }
            }
        });

    }

    public void signIn() {
        ApiEndPoint client = ServiceGenerator.createService(ApiEndPoint.class);

        @SuppressLint("HardwareIds") String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        System.out.println(deviceId);

        String devicetoken = "asdfghjklpoiuyewq1nasd3b243hj4b21j321l4jb123jh4b65";
        Integer devicetype = 1;

        LoginRequestModel loginRequestModel = new LoginRequestModel();

        loginRequestModel.setEmail(Objects.requireNonNull(mEmailId));
        loginRequestModel.setPassword(Objects.requireNonNull(mPassword));
        loginRequestModel.setDeviceID(deviceId);
        loginRequestModel.setDeviceToken(devicetoken);
        loginRequestModel.setDeviceType(devicetype);

        Call<LoginResponseModel> response = client.getLoginResponse(loginRequestModel);
        response.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {
                response.isSuccessful();

                Hawk.init(getApplicationContext()).build();
                Hawk.put("loginDetails", response.body());
                Hawk.put(accessToken, response.body().getResult().getAccessToken());
                Hawk.put(userid,response.body().getResult().getUser().getId());
                Intent intent = new Intent(SignIn.this, HomeActivity.class);
                startActivity(intent);
                finish();

            }

            @Override
            public void onFailure(Call<LoginResponseModel> call, Throwable t) {

                Toast.makeText(SignIn.this, "Please ckeck email/ password", Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}