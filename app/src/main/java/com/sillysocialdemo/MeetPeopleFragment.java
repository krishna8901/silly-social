package com.sillysocialdemo;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import com.orhanobut.hawk.Hawk;
import com.sillysocialdemo.Adapters.MeetPeopleAdapter;
import com.sillysocialdemo.ResponseModels.MeetPeopleResponseModel;
import com.sillysocialdemo.SillySocialServices.ApiEndPoint;
import com.sillysocialdemo.SillySocialServices.ServiceGenerator;
import com.sillysocialdemo.databinding.FragmentMeetPeopleBinding;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MeetPeopleFragment extends Fragment {

    FragmentMeetPeopleBinding binding;
    public static final String accessToken = "AToken";
    MeetPeopleAdapter adapter;

    List<MeetPeopleResponseModel.Content> list;
    List<MeetPeopleResponseModel.CommonYoutag> listt;
    Context context;

    int page=1;
    int limit=30;
    boolean isScrolling = false;
    int currentItems,scrolledItems,totalItems;
    LinearLayoutManager manager;

    public MeetPeopleFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding= DataBindingUtil.inflate(inflater,R.layout.fragment_meet_people,container,false);

        meetPeopleApiCall(page,limit);

       return binding.getRoot();
    }

    public void meetPeopleApiCall(int page, int limit){

        List<Object> hasTags = new ArrayList<>();
        ApiEndPoint client = ServiceGenerator.createService(ApiEndPoint.class);
        Hawk.init(getContext()).build();
        String accessTkn = Hawk.get(accessToken);
        Call<MeetPeopleResponseModel> responseModelCall=client.getMeetPeopleList(accessTkn,page,limit,hasTags);

        responseModelCall.enqueue(new Callback<MeetPeopleResponseModel>() {
            @Override
            public void onResponse(Call<MeetPeopleResponseModel> call, Response<MeetPeopleResponseModel> response) {
                if(response.isSuccessful()){

                    list=response.body().getResult().getContentList();
                    listt=response.body().getResult().getContentList().get(1).getCommonYoutags();

                    adapter = new MeetPeopleAdapter(list,listt,context);
                    binding.meetPeopleRv.setLayoutManager(new LinearLayoutManager(getContext()));
                    binding.meetPeopleRv.setAdapter(adapter);
//                    binding.meetPeopleRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
//                        @Override
//                        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//                            super.onScrollStateChanged(recyclerView, newState);
//                            if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
//                                isScrolling =true;
//                            }
//                        }
//
//                        @Override
//                        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                            super.onScrolled(recyclerView, dx, dy);
//                            currentItems=manager.getChildCount();
//                            totalItems=manager.getItemCount();
//                            scrolledItems=manager.findFirstVisibleItemPosition();
//                            if(isScrolling && (currentItems + scrolledItems == totalItems)){
//                                isScrolling =false;
//                                fetchData();
//                            }
//                        }
//                    });

                }
            }
            @Override
            public void onFailure(Call<MeetPeopleResponseModel> call, Throwable t) {

            }
        });
    }
//    private void fetchData(){
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//                for(int i=0;i<5; i++){
//                    list.add(Math.floor(Math.random()*100)+"");
//                    adapter.notifyDataSetChanged();
//                }
//            }
//        }, 500);
//
//    }
}