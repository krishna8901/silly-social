package com.sillysocialdemo;

import java.util.ArrayList;
import java.util.List;

public interface SelectedTheme {
    List<ThemeData> selectedItem=new ArrayList<>();

    public static void setList(List<ThemeData> value){
        selectedItem.addAll(value);

    }
    public static  void selectItem(int index){
        String themeName=selectedItem.get(index).themeName;
        selectedItem.set(index, new ThemeData(themeName,true));
    }
    public static  void removeItem(int index){
        String themeName=selectedItem.get(index).themeName;
        selectedItem.set(index, new ThemeData(themeName,true));
        selectedItem.notifyAll();
    }

}


class ThemeData{
    String themeName;
    boolean isSelected;

    public ThemeData(String themeName, boolean isSelected) {
        this.themeName = themeName;
        this.isSelected = isSelected;
    }
}