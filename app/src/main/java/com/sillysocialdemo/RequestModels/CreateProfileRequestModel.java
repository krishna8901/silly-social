package com.sillysocialdemo.RequestModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CreateProfileRequestModel {
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("youtags")
    @Expose
    private List<Integer> youtags = null;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("acceptTerm")
    @Expose
    private Integer acceptTerm;
    @SerializedName("images")
    @Expose
    private List<String> images = null;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("textStatus")
    @Expose
    private String textStatus;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public List<Integer> getYoutags() {
        return youtags;
    }

    public void setYoutags(List<Integer> youtags) {
        this.youtags = youtags;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getGender(String s) {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getAcceptTerm() {
        return acceptTerm;
    }

    public void setAcceptTerm(Integer acceptTerm) {
        this.acceptTerm = acceptTerm;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTextStatus() {
        return textStatus;
    }

    public void setTextStatus(String textStatus) {
        this.textStatus = textStatus;
    }
}
