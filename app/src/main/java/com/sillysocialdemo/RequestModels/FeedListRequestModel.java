package com.sillysocialdemo.RequestModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeedListRequestModel {

    @SerializedName("filterType")
    @Expose
    private String filterType;
    @SerializedName("limit")
    @Expose
    private int limit;
    @SerializedName("page")
    @Expose
    private int page;
    @SerializedName("userId")
    @Expose
    private int userId;

    public String getFilterType() {
        return filterType;
    }

    public void setFilterType(int filterType) {
        this.filterType = String.valueOf(filterType);
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
