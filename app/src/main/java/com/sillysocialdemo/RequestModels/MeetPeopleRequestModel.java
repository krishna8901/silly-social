package com.sillysocialdemo.RequestModels;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MeetPeopleRequestModel {

        @SerializedName("limit")
        @Expose
        private String limit;
        @SerializedName("page")
        @Expose
        private String page;
        @SerializedName("youtags")
        @Expose
        private List<String> youtags = null;
        @SerializedName("hashtags")
        @Expose
        private List<Integer> hashtags = null;

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        public String getPage() {
            return page;
        }

        public void setPage(String page) {
            this.page = page;
        }

        public List<String> getYoutags() {
            return youtags;
        }

        public void setYoutags(List<String> youtags) {
            this.youtags = youtags;
        }

        public List<Integer> getHashtags() {
            return hashtags;
        }

        public void setHashtags(List<Integer> hashtags) {
            this.hashtags = hashtags;
        }
    }
