package com.sillysocialdemo.SillySocialServices;

public class ValidationService {

    //private static final String passwordPattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,}";
    public static String emailPattern = "[a-zA-Z0-9._+-]+@[a-z]+\\.+[a-z]+";
    private static String userPattern = "(?=.*[a-z])(?=.*)";
    public static String pass = "(.*[0-9].*)\")(.*[a-z].*)\") (.*[A-Z].*)\") (.*[A-Z].*)\") ^(?=.*[_.#*^%!()$&@]).*$\")";

    public static boolean emptyEmail(String text){
        if(text.isEmpty()){
            return true;
        }else {
            return false;
        }
    }

    public static boolean emptyPassword(String text){
        if(text.isEmpty()){
            return true;
        }else {
            return false;
        }
    }

    public static boolean validEmail(String text){
        if (text.matches(emailPattern)){
            return true;
        }else {
            return false;
        }
    }

    public static boolean validPassword(String text){
        if(text.length() >= 5){
            return true;
        }else {
            return false;
        }
    }
    public static boolean passLength(String text){
        if (text.length()<=5){
            return true;
        }else {
            return false;
        }
    }
}
