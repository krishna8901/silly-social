package com.sillysocialdemo.SillySocialServices;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    public static String getBaseUrl() {
        return BASE_URL;
    }

    private static final String BASE_URL = "http://54.237.101.105/api/";


    private static Gson gson = new GsonBuilder().setLenient()
            .serializeNulls()
            .create();

    private static OkHttpClient client = new OkHttpClient.Builder()
            //    .addInterceptor(new MyInterceptor())
            .connectTimeout(100, TimeUnit.SECONDS)
            .writeTimeout(100, TimeUnit.SECONDS)
            .readTimeout(100, TimeUnit.SECONDS).build();


    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson));

    private static Retrofit retrofit = builder.build();


    private static final HttpLoggingInterceptor loggingInterceptor = new
            HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

    private static final OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();

        public static <S> S createService(Class<S> serviceClass) {

        if (!httpClientBuilder.interceptors().contains(loggingInterceptor)) {
            httpClientBuilder.addInterceptor(loggingInterceptor);
            builder = builder.client(httpClientBuilder.build());
            retrofit = builder.build();
        }
        return retrofit.create(serviceClass);
    }
//}
//    public static <S> S createService(Class<S> serviceClass, String baseUrl) {
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(baseUrl)
//                .client(client)
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .build();
//        return retrofit.create(serviceClass);
//    }

}