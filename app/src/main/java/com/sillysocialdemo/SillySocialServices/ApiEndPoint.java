package com.sillysocialdemo.SillySocialServices;

import com.sillysocialdemo.RequestModels.FeedListRequestModel;
import com.sillysocialdemo.RequestModels.SignUpRequestModel;
import com.sillysocialdemo.RequestModels.ViewProfileRequestModel;
import com.sillysocialdemo.ResponseModels.FeedListResponseModel;
import com.sillysocialdemo.ResponseModels.MeetPeopleResponseModel;
import com.sillysocialdemo.ResponseModels.SignUpResponseModel;
import com.sillysocialdemo.RequestModels.CreateProfileRequestModel;
import com.sillysocialdemo.ResponseModels.CreateProfileResponseModel;
import com.sillysocialdemo.RequestModels.ForgotPasswordRequestModel;
import com.sillysocialdemo.ResponseModels.ForgotPasswordResponseModel;
import com.sillysocialdemo.RequestModels.LoginRequestModel;
import com.sillysocialdemo.ResponseModels.LoginResponseModel;
import com.sillysocialdemo.ResponseModels.ThemeListResponseModel;
import com.sillysocialdemo.ResponseModels.ViewProfileResponseModel;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiEndPoint {

    @POST("login-user/")
    Call<LoginResponseModel> getLoginResponse(@Body LoginRequestModel loginRequestModel);

    @POST("register-user/")
    Call<SignUpResponseModel> getSignUpResponse(@Body SignUpRequestModel signUpRequestModel);

    @POST("create-profile/")
    Call<CreateProfileResponseModel> getCreateResponse(@Header("accessToken") String accessToken, @Body CreateProfileRequestModel requestModel);

    @POST("forgot-password/")
    Call<ForgotPasswordResponseModel> getForgotPassword(@Body ForgotPasswordRequestModel requestModel);

//    @POST("get-feed-list/")
//    Call<ThemeResponseModel> getThemeList(@Header("accessToken")  String accesTokrnn, @Body ThemeRequestModel requestModel);

    @GET("youtag-list/")
    Call<ThemeListResponseModel> getThemlist(@Header("accessToken") String accesTokrnn, @Query("limit") int limit, @Query("page") int page);


    @Multipart
    @POST("matched-youtag-people-list/")
    Call<MeetPeopleResponseModel> getMeetPeopleList(
            @Header("accessToken") String accesTokrnn,
            @Part("page") int page,
            @Part("limit") int limit,
        //    @Part("youTags") List<MeetPeopleResponseModel.CommonYoutag> youTags,
            @Part("hasTags") List<Object> hasTags);

    @POST("view-profile/")
    Call<ViewProfileResponseModel> getViewProfile(@Header("accessToken") String accessToken, @Body ViewProfileRequestModel requestModel);


    @POST("get-feed-list/")
    Call<FeedListResponseModel> getFeedResponse(@Header("accessToken") String accessToken, @Body FeedListRequestModel requestModel);

}